#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN = 20;        // Maximum any password will be
const int HASH_LEN = 33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char * targetHash, char * dictionaryFilename)
{
    // Open the dictionary file
    FILE *foPtr = fopen(dictionaryFilename, "r");
        if(!foPtr){
            fprintf(stderr, "Can't open dictionary for reading.");
            exit(1);
        }
    char currentWord[PASS_LEN];
    char *currentHash;
    char *currChar;
    while(fgets(currentWord,PASS_LEN, foPtr)){
    // Loop through the dictionary file, one line
    // at a time.
        currChar= strchr(currentWord, '\n');
        if(currChar){
            *currChar = '\0';
        }
    // Hash each password. Compare to the target hash.
        currentHash = md5(currentWord, strlen(currentWord));
        if(strcmp(targetHash, currentHash) == 0){
            char *returnedWord = (char*)malloc(PASS_LEN * 2 + 1);
            returnedWord = currentWord;
            // Close the dictionary file. Free up memory
            free(currentHash);
            fclose(foPtr);
            // If they match, return the corresponding password.
            return returnedWord;
        }
        free(currentHash);
    }
    return NULL;

}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        fprintf(stderr, "Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *foPtr = fopen(argv[1], "r");
    if(!foPtr){
        fprintf(stderr, "Can't open the hash file for reading.");
        exit(1);
    }
    // For each hash, crack it by passing it to crackHash
    char currentHash[HASH_LEN+1]; //Hash is 32 in length, new line is 33, null is at 34
    char *password;
    char *currChar;
    while(fgets(currentHash, HASH_LEN+1, foPtr)){
        currChar= strchr(currentHash, '\n');
        if(currChar){
            *currChar = '\0';
        }
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
        password = crackHash(currentHash, argv[2]);
        printf("%s %s\n", currentHash, password);
    }
    // Close the hash file
    fclose(foPtr);
    // Free up any malloc'd memory?
}
